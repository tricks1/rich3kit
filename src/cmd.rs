mod bb_cmd;
mod cald_cmd;
mod card_cmd;
mod ccgame_cmd;
mod chance_cmd;
pub mod common;
mod date_cmd;
mod face_cmd;
mod gop_cmd;
mod help_cmd;
mod map_cmd;
mod mouse_cmd;
mod news_cmd;
mod ppcc_cmd;
mod ppg_cmd;
mod ppm_cmd;
mod pps_cmd;
mod ppw_cmd;
mod pvs_cmd;
mod saywin_cmd;
mod screen_cmd;
mod test_cmd;
mod voc_cmd;
mod winppsay_cmd;
mod wintab_cmd;
use cald_cmd::CaldCmd;
use card_cmd::CardCmd;
use chance_cmd::ChanceCmd;
use date_cmd::DateCmd;
use gop_cmd::GOPCmd;
use help_cmd::HelpCmd;
use map_cmd::MapCmd;
use news_cmd::NewsCmd;
use ppm_cmd::PPMCmd;
use pps_cmd::PPSCmd;
use screen_cmd::ScreenCmd;
use std::collections::HashMap;
use test_cmd::TestCmd;

use self::{
    bb_cmd::BBCmd, ccgame_cmd::CCGameCmd, face_cmd::FaceCmd, mouse_cmd::MouseCmd,
    ppcc_cmd::PPCCCmd, ppg_cmd::PPGCmd, ppw_cmd::PPWCmd, pvs_cmd::PVSCmd, saywin_cmd::SayWinCmd,
    voc_cmd::VOCCmd, winppsay_cmd::WinPPSayCmd, wintab_cmd::WinTabCmd,
};
pub trait Cmd {
    fn get_name(&self) -> &'static str;
    fn execute(&self, args: &Vec<String>);
    fn get_description(&self) -> &'static str;
    fn print_help(&self);
}
pub struct CmdMap {
    cmd_map: HashMap<&'static str, Box<dyn Cmd>>,
    cmd_vec: Vec<&'static str>,
}

pub fn print_info() {
    let description = env!("CARGO_PKG_DESCRIPTION");
    let version = env!("CARGO_PKG_VERSION");
    let homepage = env!("CARGO_PKG_HOMEPAGE");
    println!("{}, v{}", description, version);
    println!("{}", homepage);
}

impl CmdMap {
    pub fn new() -> CmdMap {
        let mut map = CmdMap {
            cmd_map: HashMap::new(),
            cmd_vec: Vec::new(),
        };
        map.init();
        map
    }

    fn add(&mut self, cmd: Box<dyn Cmd>) {
        self.cmd_vec.push(cmd.get_name());
        self.cmd_map.insert(cmd.get_name(), cmd);
    }

    fn init(&mut self) {
        self.add(HelpCmd::new());
        self.add(CaldCmd::new());
        self.add(DateCmd::new());
        self.add(NewsCmd::new());
        self.add(GOPCmd::new());
        self.add(ChanceCmd::new());
        self.add(CardCmd::new());
        self.add(MapCmd::new());
        self.add(ScreenCmd::new());
        self.add(TestCmd::new());
        self.add(PPMCmd::new());
        self.add(PPSCmd::new());
        self.add(SayWinCmd::new());
        self.add(WinPPSayCmd::new());
        self.add(BBCmd::new());
        self.add(CCGameCmd::new());
        self.add(PPCCCmd::new());
        self.add(PPGCmd::new());
        self.add(PPWCmd::new());
        self.add(WinTabCmd::new());
        self.add(MouseCmd::new());
        self.add(FaceCmd::new());
        self.add(PVSCmd::new());
        self.add(VOCCmd::new());
    }

    fn list_cmd(&self) {
        println!("available command:");
        for &cmd in &self.cmd_vec {
            self.cmd_map.get(cmd).map(|c| {
                println!("\t{}: \t{}", cmd, c.get_description());
            });
        }
    }

    pub fn run(&self, args: &Vec<String>) {
        if args.len() < 2 {
            print_info();
            self.list_cmd();
            return;
        }

        if args[1].eq("help") {
            args.get(2).map_or_else(
                || {
                    self.list_cmd();
                },
                |cmd| {
                    self.cmd_map.get(cmd.as_str()).map_or_else(
                        || {
                            println!("{} not found", cmd);
                        },
                        |c| {
                            println!("{}: {}", cmd, c.get_description());
                            c.print_help();
                        },
                    );
                },
            );
            return;
        }

        args.get(1).map(|a| {
            self.cmd_map.get(a.as_str()).map(|c| {
                c.execute(args);
            });
        });
    }
}
