use std::convert::TryInto;

use crate::cmd::common::file_reader;

use super::common::four_planes;
use super::common::pallete::Pallete;
use super::common::sprite_reader::SpriteReader;
use super::Cmd;

pub struct FaceCmd {}

impl Cmd for FaceCmd {
    fn get_name(&self) -> &'static str {
        "face"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract face image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

fn get_u32(vec: &[u8], index: usize) -> Option<u32> {
    let start = index;
    let end = index + 4;
    let result = vec[start..end].try_into().ok()?;
    Some(u32::from_le_bytes(result))
}

fn get_face_vec(content: &[u8]) -> Option<Vec<Vec<u8>>> {
    let mut offset_list = Vec::new();
    let content_len = content.len();
    let tmkf_len = get_u32(content, 0)? as usize;
    for i in 0..(tmkf_len + 1) {
        let tmkf_offset = get_u32(content, (i + 1) * 4)? as usize * 4;
        if tmkf_offset >= content_len {
            offset_list.push(content_len);
            break;
        }
        offset_list.push(tmkf_offset);
    }
    let mut result = vec![];
    for i in 0..(offset_list.len() - 1) {
        let start = offset_list[i];
        let end = offset_list[i + 1];
        let vec = content[start..end].to_vec();
        result.push(vec);
    }
    Some(result)
}

impl FaceCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(FaceCmd {})
    }

    pub fn extract(&self, path: &String) {
        let reader = SpriteReader::new();
        let pallete = Pallete::from(&format!("{}/16.PAT", path));
        let face_path = format!("{}/FACE.PCV", path);
        let face_content = file_reader::from(&face_path);
        let tmkf_list = get_face_vec(&face_content).unwrap();
        for j in 0..tmkf_list.len() {
            let tmkf = &tmkf_list[j];
            let buffer_1 = reader.get_four_planes_buffer(tmkf, 0);
            let buffer_2 = reader.get_four_planes_buffer(tmkf, 5);
            let image_1 = buffer_1.extract_image(&pallete);
            let image_2 = buffer_2.extract_image(&pallete);
            four_planes::extract_from_2_image_buffers(
                &image_1,
                &image_2,
                format!("face-{}.png", j + 1),
            );
        }
    }
}
