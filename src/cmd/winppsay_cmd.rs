use super::common::four_planes;
use super::common::mkf::MKF;
use super::common::pallete::Pallete;
use super::common::sprite_reader::SpriteReader;
use super::Cmd;

pub struct WinPPSayCmd {}

impl Cmd for WinPPSayCmd {
    fn get_name(&self) -> &'static str {
        "winppsay"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract winppsay image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

impl WinPPSayCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(WinPPSayCmd {})
    }

    pub fn extract(&self, path: &String) {
        let ppm_path = format!("{}/WINPPSAY.MKF", path);
        let mkf = MKF::from(&ppm_path);
        let reader = SpriteReader::new();
        let pallete = Pallete::from(&format!("{}/16.PAT", path));

        for i in 0..mkf.get_smkf_len() {
            let smkf = mkf.get_smkf(i).unwrap();
            let buffer_1 = reader.get_four_planes_buffer(smkf, 0);
            let buffer_2 = reader.get_four_planes_buffer(smkf, 5);
            let image_1 = buffer_1.extract_image(&pallete);
            let image_2 = buffer_2.extract_image(&pallete);
            four_planes::extract_from_2_image_buffers(
                &image_1,
                &image_2,
                format!("winppsay-{}.png", i + 1),
            );
            //image_1.save(format!("saywin-{}-a.png", i + 1)).unwrap();
            //image_2.save(format!("saywin-{}-b.png", i + 1)).unwrap();
        }
    }
}
