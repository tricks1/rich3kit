use super::Cmd;
pub struct TestCmd {}

fn test(_: &String) {}

impl Cmd for TestCmd {
    fn get_name(&self) -> &'static str {
        "test"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                panic!("need path");
            },
            |path| {
                test(path);
            },
        )
    }

    fn get_description(&self) -> &'static str {
        "this intended to test only"
    }

    fn print_help(&self) {
        println!("this is test only");
    }
}

impl TestCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(TestCmd {})
    }
}
