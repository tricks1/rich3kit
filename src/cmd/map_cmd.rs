use std::convert::TryInto;

use super::common::constant;
use super::common::mkf::MKF;
use super::common::pallete::Pallete;
use super::Cmd;
use image::Rgb;
use image::{GenericImage, ImageBuffer};

pub struct MapCmd {}

impl Cmd for MapCmd {
    fn get_name(&self) -> &'static str {
        "map"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(3).map_or_else(
            || {
                println!("please specify path and map name");
            },
            |path| {
                let mkf_path = format!("{}/MAP.MKF", path);
                let mkf = MKF::from(&mkf_path);
                let map_name = args.get(2).unwrap();
                let (map, tiles) = match map_name.as_str() {
                    "taipei" => (0, 1),
                    "taiwan" => (20, 21),
                    "china" => (40, 41),
                    _ => panic!("unkown map name"),
                };
                let pallete = Pallete::from(&format!("{}/16.PAT", path));
                self.extract(&mkf, &pallete, tiles, map, map_name.as_str());
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract map image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit map <taipei|taiwan|china> <rich3 resource path>");
    }
}

impl MapCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(MapCmd {})
    }

    fn extract(
        &self,
        mkf: &MKF,
        pallete: &Pallete,
        tile_smfk: usize,
        map_smfk: usize,
        image_path: &str,
    ) {
        let image_size = constant::TILE_SIZE as u32;
        let plane_width = image_size / 8;
        let plane_height = image_size;

        let tile_vec: Vec<ImageBuffer<Rgb<u8>, _>> = mkf
            .to_four_planes_vec(tile_smfk, plane_width, plane_height)
            .iter()
            .map(|x| x.extract_image(pallete))
            .collect();

        let map_buffer = mkf.get_smkf(map_smfk).unwrap();
        let map_tile_len = constant::MAP_TILE_LEN as u32;
        let map_tile_count = map_tile_len * map_tile_len;
        let tile_byte_len = 2;
        let map_image_size = image_size * map_tile_len;
        let mut map_image: ImageBuffer<Rgb<u8>, _> =
            ImageBuffer::new(map_image_size, map_image_size);
        for i in 0..map_tile_count {
            let index_byte_start = (i * tile_byte_len) as usize;
            let index_byte_end = index_byte_start + tile_byte_len as usize;
            let index = u16::from_le_bytes(
                map_buffer[index_byte_start..index_byte_end]
                    .try_into()
                    .unwrap(),
            ) as usize;
            let tile_image = &tile_vec[index];
            let tile_index_x = i % map_tile_len;
            let tile_index_y = i / map_tile_len;
            let x = tile_index_x * image_size;
            let y = tile_index_y * image_size;
            map_image.copy_from(tile_image, x, y).unwrap();
        }
        map_image.save(format!("{}.png", image_path)).unwrap();
    }
}
