use std::{fs::File, io::Write};

use super::{common::mkf::MKF, Cmd};

pub struct VOCCmd {}

impl Cmd for VOCCmd {
    fn get_name(&self) -> &'static str {
        "voc"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || println!("please specify rich3 folder"),
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract voc files"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

impl VOCCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(VOCCmd {})
    }

    pub fn extract(&self, path: &String) {
        let mkf = MKF::from(path);
        for i in 0..mkf.get_smkf_len() {
            let smkf = mkf.get_smkf(i).unwrap();
            let file_name = format!("{}-{}.voc", mkf.get_file_name(), i);
            let mut file = File::create(file_name).unwrap();
            file.write_all(smkf.as_slice()).unwrap();
        }
    }
}
