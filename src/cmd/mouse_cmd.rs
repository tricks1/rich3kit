use crate::cmd::common::four_planes;
use crate::cmd::common::four_planes_reader::FourPlanes;

use super::common::data_buffer::DataBuffer;
use super::common::mkf::MKF;
use super::common::pallete::Pallete;
use super::Cmd;

pub struct MouseCmd {}

impl Cmd for MouseCmd {
    fn get_name(&self) -> &'static str {
        "mouse"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract mouse image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

impl MouseCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(MouseCmd {})
    }

    pub fn extract(&self, path: &String) {
        let mkf_path = format!("{}/MOUSE.MKF", path);
        let mkf = MKF::from(&mkf_path);
        let pallete = Pallete::from(&format!("{}/16.PAT", path));

        for i in 0..mkf.get_smkf_len() {
            let tmkf_list = mkf.get_tmkf_list(i).unwrap();
            for j in 0..tmkf_list.len() {
                //println!("i: {}, j: {}", i, j);
                let tmkf = &tmkf_list[j];
                MouseCmd::extract_tmkf(tmkf, i, j, &pallete).map_or_else(
                    || {
                        println!("failed to extract mouse-{}-{}", i, j);
                    },
                    |_| {
                        println!("succeeded to extract mouse-{}-{}", i, j);
                    },
                );
            }
        }
    }

    pub fn extract_tmkf(tmkf: &Vec<u8>, i: usize, j: usize, pallete: &Pallete) -> Option<()> {
        let mut data_buffer = DataBuffer::new(&tmkf);

        let plane_width = data_buffer.get_u16().unwrap() as usize;
        let plane_height = data_buffer.get_u16().unwrap() as usize;
        println!("plane width: {}, height: {}", plane_width, plane_height);
        let plane_len = plane_width * plane_height;

        let vec_0 = get_image_vec(0, plane_len, plane_height, plane_width, &mut data_buffer);
        let vec_1 = get_image_vec(0xff, plane_len, plane_height, plane_width, &mut data_buffer);

        let four_planes_0 = FourPlanes::new(
            plane_width as u16,
            plane_height as u16,
            format!("mouse-{}-{}", i, j),
            &vec_0,
        );
        let four_planes_1 = FourPlanes::new(
            plane_width as u16,
            plane_height as u16,
            format!("mouse-{}-{}", i, j),
            &vec_1,
        );

        let img_0 = four_planes_0.extract_image(pallete);
        let img_1 = four_planes_1.extract_image(pallete);

        four_planes::extract_from_2_image_buffers(&img_0, &img_1, format!("mouse-{}-{}.png", i, j));
        Some(())
    }
}

fn get_image_vec(
    bg: u8,
    plane_len: usize,
    plane_height: usize,
    plane_width: usize,
    data_buffer: &mut DataBuffer,
) -> Vec<u8> {
    data_buffer.set_current(4);
    let mut vec = vec![0; 4 * plane_len];
    for h in 0..plane_height {
        let mut mask_vec = Vec::new();
        for _ in 0..plane_width {
            mask_vec.push(data_buffer.get_u8().unwrap());
        }
        for i in 0..4 {
            for w in 0..plane_width {
                let mask = mask_vec[w];
                let d = data_buffer.get_u8().unwrap();
                vec[i * plane_len + w + plane_width * h] = bg & mask | d;
            }
        }
    }
    vec
}
