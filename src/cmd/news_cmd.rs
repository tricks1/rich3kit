use super::common::bulk;
use super::Cmd;
pub struct NewsCmd {}

impl Cmd for NewsCmd {
    fn get_name(&self) -> &'static str {
        "news"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {},
            |path| {
                bulk::extract(path, "news", "NEWS.BLK");
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract news image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit news <rich3 resource path>");
    }
}

impl NewsCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(NewsCmd {})
    }
}
