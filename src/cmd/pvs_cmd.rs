use std::fs::{self, File};
use std::path::PathBuf;

use image::gif::{GifEncoder, Repeat};
use image::{DynamicImage, Frame};

use crate::cmd::common::pallete::Pallete;
use crate::cmd::common::{data_buffer::DataBuffer, file_reader, four_planes::Buffer};

use super::{common::screen_reader::MethodMap, Cmd};

pub struct PVSCmd {
    method_map: MethodMap,
}

impl Cmd for PVSCmd {
    fn get_name(&self) -> &'static str {
        "pvs"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract_all(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract pvs image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

impl PVSCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(PVSCmd {
            method_map: MethodMap::new(),
        })
    }

    fn extract_all(&self, folder: &str) {
        let path_buf = PathBuf::from(folder);
        for read_result in fs::read_dir(path_buf).unwrap() {
            let entry = read_result.unwrap();
            let path = entry.path();
            if let Some(file_name_str) = path.file_name() {
                if let Some(ext_name_str) = path.extension() {
                    if ext_name_str.to_ascii_lowercase() != "pvs" {
                        continue;
                    }
                    let full_path = fs::canonicalize(&path).unwrap();
                    let file_name = file_name_str.to_str().unwrap();
                    let file_name_len = file_name.len();
                    println!(
                        "name: {}, ext: {:#?}, path: {:#?}",
                        &file_name[0..(file_name_len - 4)],
                        ext_name_str,
                        full_path
                    );
                    self.extract_file(full_path, &file_name[0..(file_name_len - 4)]);
                }
            }
        }
    }

    pub fn extract_file(&self, path: PathBuf, file_name: &str) {
        let content = file_reader::from_path_buffer(&path);
        let pallete = Pallete::from_bytes(&content[4..0x34]);
        // data start from 0034
        let mut file_buffer = DataBuffer::new(&content);
        file_buffer.set_current(0x34);
        let mut buffer = Buffer::default();
        let mut image_count = 0;
        let mut frame_vec = Vec::new();
        loop {
            image_count += 1;
            let image_len = file_buffer.get_u32().unwrap() as usize;
            let start = file_buffer.get_current();
            let end = start + image_len;
            println!(
                "image: {}, start: {}, end: {}, image_len: {}",
                image_count, start, end, image_len
            );
            if image_len == 0 {
                break;
            }
            if end > content.len() {
                println!("end: {}, content len: {}", end, content.len());
                break;
            }
            let image_vec = content[start..end].to_vec();
            buffer.reset_position();
            buffer.reset_index();
            self.method_map.read(&mut buffer, &image_vec);
            let image = buffer.extract_image(&pallete);
            let dynamic_image = DynamicImage::ImageRgb8(image);
            let frame = Frame::new(dynamic_image.to_rgba8());
            frame_vec.push(frame);

            file_buffer.offset(image_len);
            if !file_buffer.is_valid_current() {
                break;
            }
        }
        println!("file len: {}", content.len());
        let file_out = File::create(format!("{}.gif", file_name).to_ascii_lowercase()).unwrap();
        let mut encoder = GifEncoder::new(file_out);
        encoder.set_repeat(Repeat::Infinite).unwrap();
        encoder.encode_frames(frame_vec).unwrap();
    }
}
