use super::common::constant;
use super::common::gop;
use super::common::pallete::Pallete;
use super::Cmd;
pub struct GOPCmd {}

impl Cmd for GOPCmd {
    fn get_name(&self) -> &'static str {
        "gop"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(3).map_or_else(
            || {
                panic!("please specify gop file name and resource path");
            },
            |path| {
                let gop_file = args.get(2).unwrap();
                let gop_file_upper = gop_file.to_uppercase();
                let gop_path = format!("{}/{}.GOP", path, gop_file_upper);
                let pallete = Pallete::from(&format!("{}/16.PAT", path));
                let image_size = match gop_file_upper.as_str() {
                    "BASE" => constant::TILE_SIZE,
                    "SHOUSE" => constant::TILE_SHOUSE_SIZE,
                    _ => panic!("image size for {} is not known", gop_file),
                };
                gop::extract(&gop_path, gop_file.as_str(), &pallete, image_size);
            },
        )
    }

    fn get_description(&self) -> &'static str {
        "extract gop image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit gop <BASE | SHOUSE> <rich3 resource path>");
    }
}

impl GOPCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(GOPCmd {})
    }
}
