use super::common::bulk;
use super::Cmd;

pub struct DateCmd {}

impl Cmd for DateCmd {
    fn get_name(&self) -> &'static str {
        "date"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract date image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit date <rich3 resource path>");
    }
}

impl DateCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(DateCmd {})
    }

    pub fn extract(&self, path: &String) {
        bulk::extract(path, "date", "DATE.BLK");
    }
}
