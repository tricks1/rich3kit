use crate::cmd::Cmd;
pub struct HelpCmd {}

impl Cmd for HelpCmd {
    fn get_name(&self) -> &'static str {
        "test"
    }

    fn execute(&self, _: &Vec<String>) {}

    fn get_description(&self) -> &'static str {
        "this is only a test"
    }

    fn print_help(&self) {
        println!("this command is intended to test only");
    }
}

impl HelpCmd {
    pub fn new() -> Box<HelpCmd> {
        Box::new(HelpCmd {})
    }
}
