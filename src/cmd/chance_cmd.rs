use super::common::bulk;
use super::Cmd;

pub struct ChanceCmd {}

impl Cmd for ChanceCmd {
    fn get_name(&self) -> &'static str {
        "chance"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {},
            |path| {
                bulk::extract(path, "chance", "CHANCE.BLK");
            },
        )
    }

    fn get_description(&self) -> &'static str {
        "extract description image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit chance <rich3 resource path>");
    }
}

impl ChanceCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(ChanceCmd {})
    }
}
