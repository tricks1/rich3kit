use super::common::four_planes;
use super::common::mkf::MKF;
use super::common::pallete::Pallete;
use super::common::sprite_reader::SpriteReader;
use super::Cmd;

pub struct PPMCmd {}

impl Cmd for PPMCmd {
    fn get_name(&self) -> &'static str {
        "ppm"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract ppm image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit ppm <rich3 resource path>");
    }
}

impl PPMCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(PPMCmd {})
    }

    pub fn extract(&self, path: &String) {
        let ppm_path = format!("{}/PPM.MKF", path);
        let mkf = MKF::from(&ppm_path);
        let reader = SpriteReader::new();
        let pallete = Pallete::from(&format!("{}/16.PAT", path));

        for i in 0..mkf.get_smkf_len() {
            let smkf = mkf.get_smkf(i).unwrap();
            let buffer_1 = reader.get_four_planes_buffer(smkf, 0);
            let buffer_2 = reader.get_four_planes_buffer(smkf, 4);
            let image_1 = buffer_1.extract_image(&pallete);
            let image_2 = buffer_2.extract_image(&pallete);
            four_planes::extract_from_2_image_buffers(
                &image_1,
                &image_2,
                format!("ppm-{}.png", i + 1),
            );
        }
    }
}
