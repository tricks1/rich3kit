use super::common::bulk;
use super::Cmd;

pub struct CardCmd {}

impl Cmd for CardCmd {
    fn get_name(&self) -> &'static str {
        "card"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {},
            |path| {
                bulk::extract(path, "card", "CARD.BLK");
            },
        );
    }
    fn get_description(&self) -> &'static str {
        "extract card image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit card <rich3 resource path>");
    }
}

impl CardCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(CardCmd {})
    }
}
