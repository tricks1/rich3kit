use super::common::four_planes;
use super::common::mkf::MKF;
use super::common::pallete::Pallete;
use super::common::sprite_reader::SpriteReader;
use super::Cmd;

pub struct CCGameCmd {}

impl Cmd for CCGameCmd {
    fn get_name(&self) -> &'static str {
        "ccgame"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                println!("extracting from {}", path);
                self.extract(path);
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract ccgame image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit {} <rich3 resource path>", self.get_name());
    }
}

impl CCGameCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(CCGameCmd {})
    }

    pub fn extract(&self, path: &String) {
        let mkf_path = format!("{}/CCGAME.MKF", path);
        let mkf = MKF::from(&mkf_path);
        let reader = SpriteReader::new();
        let pallete = Pallete::from(&format!("{}/16.PAT", path));

        for i in 0..mkf.get_smkf_len() {
            let tmkf_list = mkf.get_tmkf_list(i).unwrap();
            for j in 0..tmkf_list.len() {
                //println!("i: {}, j: {}", i, j);
                let tmkf = &tmkf_list[j];
                let buffer_1 = reader.get_four_planes_buffer(tmkf, 0);
                let buffer_2 = reader.get_four_planes_buffer(tmkf, 5);
                let image_1 = buffer_1.extract_image(&pallete);
                let image_2 = buffer_2.extract_image(&pallete);
                four_planes::extract_from_2_image_buffers(
                    &image_1,
                    &image_2,
                    format!("ccgame-{}-{}.png", i + 1, j + 1),
                );
            }
        }
    }
}
