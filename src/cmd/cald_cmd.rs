use crate::cmd::Cmd;
extern crate chinese_lunisolar_calendar;
use chinese_lunisolar_calendar::chrono::prelude::*;
use chinese_lunisolar_calendar::SolarDate;
use std::fs::File;
use std::io::prelude::*;
use std::str::FromStr;
pub struct CaldCmd {}

impl Cmd for CaldCmd {
    fn get_name(&self) -> &'static str {
        "cald"
    }

    fn execute(&self, args: &Vec<String>) {
        let day_count = 9132usize;
        let start_year_arg = 2021;
        let start_month_arg = 1;
        let start_day_arg = 1;
        let start_date = if args.len() < 3 {
            println!(
                "start date not specified, starting from {}-{}-{} by default",
                start_year_arg, start_month_arg, start_day_arg
            );
            NaiveDate::from_ymd(start_year_arg, start_month_arg, start_day_arg)
        } else {
            NaiveDate::from_str(args[2].as_str()).unwrap()
        };

        let start_days_from_ce = start_date.num_days_from_ce();
        let mut cald_a_buffer = Vec::new();
        let mut cald_b_buffer = Vec::new();
        for i in 0..day_count {
            let current_days = start_days_from_ce + i as i32;
            let current_naive_date = NaiveDate::from_num_days_from_ce(current_days);
            let current_solar_date = SolarDate::from_naive_date(current_naive_date).unwrap();

            cald_a_buffer.push(current_naive_date.day() as u8);
            cald_a_buffer.push(current_naive_date.month() as u8);
            for u in (current_naive_date.year() as u16).to_le_bytes().iter() {
                cald_a_buffer.push(*u);
            }

            let current_lunar_date = current_solar_date.to_lunisolar_date().unwrap();
            let current_lunar_year = current_lunar_date
                .get_lunisolar_year()
                .to_solar_year()
                .to_u16();
            let current_lunar_month = current_lunar_date.get_lunar_month().to_u8();
            let current_lunar_day = current_lunar_date.get_lunar_day().to_u8();
            cald_b_buffer.push(current_lunar_day);
            cald_b_buffer.push(current_lunar_month);
            let current_lunar_year_byte = current_lunar_year.to_le_bytes();
            for b in current_lunar_year_byte.iter() {
                cald_b_buffer.push(*b);
            }
        }

        let date_string = format!(
            "{}-{}-{}",
            start_date.year(),
            start_date.month(),
            start_date.day()
        );
        let cald_a_filename = format!("CALD-{}.A", date_string);
        let mut cald_a_new_file = File::create(cald_a_filename).unwrap();
        cald_a_new_file.write_all(cald_a_buffer.as_slice()).unwrap();

        let cald_b_filename = format!("CALD-{}.B", date_string);
        let mut cald_b_new_file = File::create(cald_b_filename).unwrap();
        cald_b_new_file.write_all(cald_b_buffer.as_slice()).unwrap();
    }

    fn get_description(&self) -> &'static str {
        "create new cald.a and cald.b"
    }

    fn print_help(&self) {
        println!("{} from the date specified", self.get_description());
        println!("usage:");
        println!("\t rich3kit cald [yyyy-mm-dd]");
    }
}

impl CaldCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(CaldCmd {})
    }
}
