use super::common::screen_reader::Screen;
use super::Cmd;
pub struct ScreenCmd {}

impl Cmd for ScreenCmd {
    fn get_name(&self) -> &'static str {
        "screen"
    }

    fn execute(&self, args: &Vec<String>) {
        args.get(2).map_or_else(
            || {
                println!("please specify rich3 folder");
            },
            |path| {
                let screen = Screen::from(path);
                screen.extract_all();
            },
        );
    }

    fn get_description(&self) -> &'static str {
        "extract screen image"
    }

    fn print_help(&self) {
        println!("{} from the path specified", self.get_description());
        println!("usage: ");
        println!("\trich3kit screen <rich3 resource path>");
    }
}

impl ScreenCmd {
    pub fn new() -> Box<dyn Cmd> {
        Box::new(ScreenCmd {})
    }
}

#[cfg(test)]
mod tests {
    use crate::cmd::common::screen_reader::Screen;

    #[test]
    pub fn screen_test() {
        println!("it works");
    }

    #[test]
    pub fn screen_0() {
        let path = String::from("C:\\Users\\fliar\\Downloads\\tmp\\dosbox\\rich3\\");
        let screen = Screen::from(&path);
        screen.extract_to_frames(0);
    }
}
