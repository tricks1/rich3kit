use std::convert::TryInto;

pub struct DataBuffer<'a> {
    data: &'a [u8],
    current: usize,
}

impl<'a> DataBuffer<'a> {
    pub fn new(data: &[u8]) -> DataBuffer {
        DataBuffer {
            data: data,
            current: 0,
        }
    }

    pub fn is_valid_current(&self) -> bool {
        self.current < self.data.len()
    }

    pub fn get_current(&self) -> usize {
        self.current
    }

    pub fn set_current(&mut self, c: usize) {
        self.current = c;
    }

    pub fn offset(&mut self, offset: usize) {
        self.current += offset;
    }

    pub fn get_u8(&mut self) -> Option<u8> {
        let result = self.data.get(self.current);
        self.current += 1;
        result.map_or_else(|| None, |x| Some(*x))
    }

    pub fn get_u16(&mut self) -> Option<u16> {
        if self.current + 1 < self.data.len() {
            let start = self.current;
            let end = self.current + 2;
            let bytes = self.data[start..end].try_into().ok()?;
            self.current += 2;
            return Some(u16::from_le_bytes(bytes));
        }
        None
    }

    pub fn get_u32(&mut self) -> Option<u32> {
        if self.current + 3 < self.data.len() {
            let start = self.current;
            let end = self.current + 4;
            let bytes = self.data[start..end].try_into().ok()?;
            self.current += 4;
            return Some(u32::from_le_bytes(bytes));
        }
        None
    }

    pub fn get_by_byte_count(&mut self, byte_count: usize) -> Option<&[u8]> {
        if self.current + byte_count < self.data.len() {
            let start = self.current;
            let end = self.current + byte_count;
            self.current += byte_count;
            return Some(&self.data[start..end]);
        }
        None
    }
}
