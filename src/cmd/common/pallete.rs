use crate::cmd::common::color::Color;
use std::fs::File;
use std::io::prelude::*;

pub struct Pallete {
    color_vec: Vec<Color>,
}

impl Pallete {
    pub fn from(path: &String) -> Pallete {
        let mut file = File::open(path).unwrap();
        let mut file_buffer: Vec<u8> = Vec::new();
        file.read_to_end(&mut file_buffer).unwrap();

        Pallete::from_bytes(&file_buffer[0..])
    }

    pub fn get_color(&self, index: usize) -> Option<&Color> {
        self.color_vec.get(index)
    }

    pub fn from_bytes(bytes: &[u8]) -> Pallete {
        let mut color_vec = Vec::new();

        for i in 0..16usize {
            let r_index = i * 3;
            let g_index = i * 3 + 1;
            let b_index = i * 3 + 2;
            color_vec.push(Color {
                r: bytes[r_index] << 2,
                g: bytes[g_index] << 2,
                b: bytes[b_index] << 2,
            });
        }
        return Pallete {
            color_vec: color_vec,
        };
    }
}
