use super::pallete::Pallete;
use image::ImageBuffer;
use image::Rgb;
pub struct FourPlanes<'b> {
    plane_width: u16,
    plane_height: u16,
    plane_len: usize,
    filename: String,
    buffer: &'b [u8],
}

impl<'b> FourPlanes<'b> {
    pub fn new(
        plane_width: u16,
        plane_height: u16,
        filename: String,
        buffer: &'b [u8],
    ) -> FourPlanes<'b> {
        FourPlanes {
            plane_width,
            plane_height,
            plane_len: plane_width as usize * plane_height as usize,
            filename,
            buffer,
        }
    }
    fn get_image_params(&self) -> (u32, u32, u32) {
        let image_width = self.plane_width as u32 * 8;
        let image_height = self.plane_height as u32;
        let image_len = image_width * image_height;
        (image_width, image_height, image_len)
    }

    pub fn extract_image(&self, pallete: &Pallete) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        let (image_width, image_height, image_len) = self.get_image_params();
        let mut image_buffer = image::ImageBuffer::new(image_width, image_height);
        for j in 0..image_len {
            let (x, y, color_index) = self.get_pixel_index(j, image_width);

            pallete.get_color(color_index as usize).map_or_else(
                || {
                    panic!("color index out of range: {}", color_index);
                },
                |c| {
                    let pixel = image_buffer.get_pixel_mut(x, y);
                    *pixel = Rgb([c.r, c.g, c.b]);
                },
            )
        }
        image_buffer
    }

    fn get_pixel_index(&self, j: u32, image_width: u32) -> (u32, u32, u8) {
        let x = j % image_width;
        let y = j / image_width;
        let plane_bit_index = 7 - j % 8;
        let plane_byte_index = j / 8;
        let plane_offset = plane_byte_index as usize;
        let mut plane_bytes = [0u8; 4];
        for k in 0..4 {
            plane_bytes[k] = self.buffer[plane_offset + self.plane_len * k];
        }
        let plane_bits: Vec<u8> = plane_bytes
            .iter()
            .map(|x| x >> plane_bit_index & 1)
            .collect();
        let color_index = plane_bits
            .iter()
            .enumerate()
            .fold(0, |sum, (k, v)| sum + (v << k));
        (x, y, color_index)
    }
    pub fn extract(&self, pallete: &Pallete) {
        self.extract_image(pallete)
            .save(format!("{}.png", self.filename))
            .unwrap();
    }
}
