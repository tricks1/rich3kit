use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

pub fn from(path: &String) -> Vec<u8> {
    let mut buf = Vec::new();
    let mut file = File::open(path).unwrap();
    file.read_to_end(&mut buf).unwrap();
    buf
}

pub fn from_path_buffer(path: &PathBuf) -> Vec<u8> {
    let mut buf = Vec::new();
    let mut file = File::open(path).unwrap();
    file.read_to_end(&mut buf).unwrap();
    buf
}
