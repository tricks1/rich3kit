use super::pallete::Pallete;
use super::screen_reader::SCREEN_HEIGHT;
use super::screen_reader::SCREEN_WIDTH;
use image::gif::GifEncoder;
use image::gif::Repeat;
use image::DynamicImage;
use image::Frame;

use image::ImageBuffer;
use image::Rgb;
use image::Rgba;
use std::convert::TryInto;
use std::fs::File;
const RATIO: usize = 8;
const PLANE_COUNT: usize = 4;

pub fn extract_from_2_image_buffers(
    image_1: &ImageBuffer<Rgb<u8>, Vec<u8>>,
    image_2: &ImageBuffer<Rgb<u8>, Vec<u8>>,
    file_name: String,
) {
    let (width, height) = image_1.dimensions();
    let mut buffer: ImageBuffer<Rgba<u8>, _> = ImageBuffer::new(width, height);

    for x in 0..width {
        for y in 0..height {
            let pixel_1 = image_1.get_pixel(x, y);
            let pixel_2 = image_2.get_pixel(x, y);
            *buffer.get_pixel_mut(x, y) = if pixel_1 == pixel_2 {
                Rgba([pixel_1[0], pixel_1[1], pixel_1[2], 0xff])
            } else {
                Rgba::from([0, 0, 0, 0])
            }
        }
    }

    buffer.save(file_name).unwrap();
}
pub struct Buffer {
    _plane_width: usize,
    _plane_height: usize,
    plane_len: usize,
    image_width: usize,
    image_height: usize,
    bg_color_index: u8,
    current_x: usize,
    current_y: usize,
    current_buffer_index: usize,
    data: Vec<u8>,
    frame_data_vec: Vec<Vec<u8>>,
}

impl Buffer {
    pub fn default() -> Buffer {
        Buffer::new(SCREEN_WIDTH, SCREEN_HEIGHT)
    }
    pub fn new(image_width: usize, image_height: usize) -> Buffer {
        let plane_width = image_width / RATIO;
        let plane_height = image_height;
        let plane_len = plane_width * plane_height;
        Buffer {
            _plane_width: plane_width,
            _plane_height: plane_height,
            plane_len,
            image_width,
            image_height,
            bg_color_index: 0,
            current_x: 0,
            current_y: 0,
            current_buffer_index: 0,
            data: vec![0; 4 * plane_len],
            frame_data_vec: Vec::new(),
        }
    }

    pub fn save_frame(&mut self) {
        self.frame_data_vec.push(self.data.clone());
    }

    fn increase_x_by(&mut self, count: usize) {
        self.current_x += count;
        /*
        while self.current_x >= self.image_width {
            self.current_y += 1;
            self.current_x -= self.image_width;
        }
        */
        //println!("current x: {}, y: {}", self.current_x, self.current_y);
    }

    pub fn get_current_location(&self) -> (usize, usize) {
        (self.current_x, self.current_y)
    }

    pub fn next_line(&mut self) {
        self.current_x %= self.image_width;
        self.current_y += 1;
    }

    fn get_pixel_index(&self) -> usize {
        self.current_y * self.image_width + self.current_x
    }

    fn get_plane_byte_index(&self) -> usize {
        self.get_pixel_index() / RATIO
    }

    pub fn get_current_u8_at(&self, buffer: &Vec<u8>, offset: usize) -> u8 {
        buffer[offset]
    }

    pub fn get_current_u8(&self, buffer: &Vec<u8>, offset: usize) -> u8 {
        buffer[self.current_buffer_index + offset]
    }

    pub fn get_current_u16(&self, buffer: &Vec<u8>, offset: usize) -> u16 {
        let start = self.current_buffer_index + offset;
        let end = start + 2;
        u16::from_le_bytes(buffer[start..end].try_into().unwrap())
    }

    pub fn set_current_buffer_index(&mut self, index: usize) {
        self.current_buffer_index = index;
    }

    pub fn get_current_buffer_index(&self) -> usize {
        self.current_buffer_index
    }

    pub fn increase_buffer_index_by(&mut self, value: usize) {
        self.current_buffer_index += value;
    }

    pub fn add_bg_color_by_byte_count(&mut self, byte_count: usize) {
        self.add_bg_color_by_count(byte_count * RATIO);
    }

    pub fn add_bg_color_by_count(&mut self, count: usize) {
        self.add_color_by_count(self.bg_color_index, count);
    }

    pub fn add_bg_color_by_byte_sprite(&mut self, byte_count: usize) {
        self.add_color_by_byte_count_with_mask(self.bg_color_index, 0xff, byte_count);
    }

    pub fn add_color_by_byte_count_with_mask(&mut self, color_index: u8, mask: u8, count: usize) {
        if color_index > 0xf {
            panic!("color index: {} out of range", color_index);
        }
        let mut color_bytes = vec![0u8; count * 4];
        for i in 0..4 {
            let color_bit = (color_index >> i) & 1;
            for j in 0..count {
                if color_bit == 1 {
                    color_bytes[j + count * i] = 0xff;
                }
            }
        }
        self.copy_byte_to_each_plane_with_mask(&color_bytes, mask);
    }

    pub fn add_color_by_byte_count(&mut self, color_index: u8, byte_count: usize) {
        let mut color_bytes = vec![0u8; byte_count * 4];
        for i in 0..4 {
            let color_bit = (color_index >> i) & 1;
            for j in 0..byte_count {
                if color_bit == 1 {
                    color_bytes[j + byte_count * i] = 0xff;
                }
            }
        }
        self.copy_byte_to_each_plane(&color_bytes);
    }

    pub fn add_color_by_count(&mut self, color_index: u8, count: usize) {
        for _ in 0..count {
            self.add_color(color_index);
        }
    }

    pub fn add_color(&mut self, color_index: u8) {
        let pixel_index = self.get_pixel_index();
        (0..4).for_each(|x| {
            let bit = (color_index >> x) & 1;
            let plane_bit_index = 7 - pixel_index % 8;
            let plane_byte_index = pixel_index / 8;
            let data_index = plane_byte_index + self.plane_len * x;

            self.data[data_index] |= bit << plane_bit_index;
        });
        self.increase_x_by(1);
    }

    pub fn copy_byte_to_each_plane_with_mask(&mut self, byte_buffer: &[u8], mask: u8) {
        let masked_byte_vec: Vec<u8> = byte_buffer.iter().map(|x| *x & mask).collect();
        self.mask_byte_to_each_plane(&masked_byte_vec[0..], mask);
    }

    pub fn mask_byte_to_each_plane(&mut self, byte_buffer: &[u8], mask: u8) {
        let byte_count = byte_buffer.len();
        if byte_count % PLANE_COUNT != 0 {
            panic!(
                "byte count: {} is not dividable by {}",
                byte_count, PLANE_COUNT
            );
        }
        let plane_len = byte_count / 4;
        let current_byte_index = self.get_plane_byte_index();
        for i in 0..PLANE_COUNT {
            let data_start = current_byte_index + i * self.plane_len;
            let buffer_start = i * plane_len;
            let reverse_mask = !mask;
            for j in 0..plane_len {
                let data_index = data_start + j;
                let masked_data = self.data[data_index] & reverse_mask;
                self.data[data_start + j] = masked_data | byte_buffer[buffer_start + j];
            }
        }
        self.increase_x_by(plane_len * RATIO);
    }

    pub fn copy_byte_to_each_plane(&mut self, byte_buffer: &[u8]) {
        let byte_count = byte_buffer.len();
        if byte_count % PLANE_COUNT != 0 {
            panic!(
                "byte count: {} is not dividable by {}",
                byte_count, PLANE_COUNT
            );
        }
        let plane_len = byte_count / 4;
        let current_byte_index = self.get_plane_byte_index();
        for i in 0..PLANE_COUNT {
            let data_start = current_byte_index + i * self.plane_len;
            let data_end = data_start + plane_len;
            let buffer_start = i * plane_len;
            let buffer_end = buffer_start + plane_len;
            self.data[data_start..data_end].copy_from_slice(&byte_buffer[buffer_start..buffer_end]);
        }
        self.increase_x_by(plane_len * RATIO);
    }

    pub fn copy_self_by_offset(&mut self, offset: usize, byte_count: usize, copy_count: usize) {
        let current_byte_index = self.get_plane_byte_index();

        for i in 0..copy_count {
            for j in 0..PLANE_COUNT {
                let buffer_start = current_byte_index - offset + j * self.plane_len;
                let buffer_end = buffer_start + byte_count;
                let data_start = current_byte_index + j * self.plane_len + i * byte_count;
                self.data.copy_within(buffer_start..buffer_end, data_start);
            }
        }
        self.increase_x_by(byte_count * copy_count * RATIO);
    }

    fn get_image_len(&self) -> usize {
        self.image_height * self.image_width
    }

    fn get_pixel_location_and_color_index(
        &self,
        pixel_index: usize,
        data: &Vec<u8>,
    ) -> (usize, usize, u8) {
        let x = pixel_index % self.image_width;
        let y = pixel_index / self.image_width;
        let plane_bit_index = 7 - pixel_index % 8;
        let plane_byte_index = pixel_index / 8;
        let plane_offset = plane_byte_index as usize;
        let mut plane_bytes = [0u8; 4];
        for k in 0..4 {
            plane_bytes[k] = data[plane_offset + self.plane_len * k];
        }
        let plane_bits: Vec<u8> = plane_bytes
            .iter()
            .map(|x| x >> plane_bit_index & 1)
            .collect();
        let color_index = plane_bits
            .iter()
            .enumerate()
            .fold(0, |sum, (k, v)| sum + (v << k));
        (x, y, color_index)
    }

    pub fn extract_frames(&self, pallete: &Pallete, file_name: String) {
        let mut frame_vec = Vec::new();
        for data in &self.frame_data_vec {
            let image = self.extract_image_data(pallete, data);
            let dynamic_image = DynamicImage::ImageRgb8(image);
            frame_vec.push(Frame::new(dynamic_image.to_rgba8()));
        }
        let file_out = File::create(file_name).unwrap();
        let mut encoder = GifEncoder::new(file_out);
        encoder.set_repeat(Repeat::Infinite).unwrap();
        encoder.encode_frames(frame_vec).unwrap();
    }

    fn extract_image_data(
        &self,
        pallete: &Pallete,
        data: &Vec<u8>,
    ) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        let mut image_buffer =
            image::ImageBuffer::new(self.image_width as u32, self.image_height as u32);

        for j in 0..self.get_image_len() {
            let (x, y, color_index) = self.get_pixel_location_and_color_index(j, data);

            pallete.get_color(color_index as usize).map_or_else(
                || {
                    panic!("color index out of range: {}", color_index);
                },
                |c| {
                    let pixel = image_buffer.get_pixel_mut(x as u32, y as u32);
                    *pixel = Rgb([c.r, c.g, c.b]);
                },
            )
        }
        image_buffer
    }

    pub fn extract_image(&self, pallete: &Pallete) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
        self.extract_image_data(pallete, &self.data)
    }

    pub fn extract_and_save(&self, pallete: &Pallete, file_name: String) {
        self.extract_image(pallete)
            .save(format!("{}.png", file_name))
            .unwrap();
    }

    pub fn set_bg_color_index(&mut self, index: u8) {
        self.bg_color_index = index;
    }

    pub fn fill_bg_color_and_reset(&mut self) {
        self.add_bg_color_by_count(self.get_image_len());
        self.reset_position()
    }

    pub fn reset_position(&mut self) {
        self.current_x = 0;
        self.current_y = 0;
    }
    pub fn reset_index(&mut self) {
        self.current_buffer_index = 0;
    }
}
