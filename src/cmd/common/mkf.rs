use super::file_reader;
use super::four_planes_reader::FourPlanes;

use std::convert::TryInto;
pub struct MKF {
    file_name: String,
    smkf_list: Vec<Vec<u8>>,
}

fn get_u32(vec: &Vec<u8>, index: usize) -> Option<u32> {
    let start = index;
    let end = index + 4;
    let result = vec[start..end].try_into().ok()?;
    Some(u32::from_le_bytes(result))
}

impl MKF {
    pub fn get_file_name(&self) -> &String {
        &self.file_name
    }
    pub fn get_tmkf_list(&self, index: usize) -> Option<Vec<Vec<u8>>> {
        let smkf = self.get_smkf(index)?;
        let smkf_len = smkf.len();
        let mut offset_list = Vec::new();
        let tmkf_len = get_u32(smkf, 0)? as usize;
        for i in 0..(tmkf_len + 1) {
            let tmkf_offset = get_u32(smkf, (i + 1) * 4)? as usize * 4;
            if tmkf_offset >= smkf_len {
                offset_list.push(smkf_len);
                break;
            }
            offset_list.push(tmkf_offset);
        }
        let mut result = vec![];
        for i in 0..(offset_list.len() - 1) {
            let start = offset_list[i];
            let end = offset_list[i + 1];
            let vec = smkf[start..end].to_vec();
            result.push(vec);
        }
        Some(result)
    }
    pub fn to_four_planes_vec<'b>(
        &'b self,
        smkf_index: usize,
        plane_width: u32,
        plane_height: u32,
    ) -> Vec<FourPlanes<'b>> {
        let mut four_planes_vec: Vec<FourPlanes<'b>> = Vec::new();
        self.smkf_list.get(smkf_index).map_or_else(
            || {
                panic!(
                    "smkf index {} out of range in {}",
                    smkf_index, self.file_name
                );
            },
            |buffer| {
                let plane_len = plane_width * plane_height;
                let block_len = 4 * plane_len as usize;
                let block_count = buffer.len() / block_len;

                for i in 0..block_count {
                    let block_start = i * block_len;
                    let block_end = block_start + block_len;
                    four_planes_vec.push(FourPlanes::new(
                        plane_width as u16,
                        plane_height as u16,
                        format!("map-tile-{}", i + 1),
                        &buffer[block_start..block_end],
                    ));
                }
            },
        );
        four_planes_vec
    }
    pub fn get_smkf_len(&self) -> usize {
        self.smkf_list.len()
    }

    pub fn get_smkf(&self, index: usize) -> Option<&Vec<u8>> {
        self.smkf_list.get(index)
    }
    pub fn from(path: &String) -> MKF {
        let converted_path = path.replace("\\", "/");
        let file_name = path.rfind("/").map_or_else(
            || converted_path.as_str(),
            |index| &converted_path[(index + 1)..],
        );

        let mkf_content = file_reader::from(path);
        let mkf_len = mkf_content.len() as u32;
        let mut current_index = 0;
        let mut offset_vec = Vec::new();
        let offset_len = 4;
        loop {
            let offset_start = current_index;
            let offset_end = offset_start + offset_len;
            let offset_bytes = mkf_content[offset_start..offset_end].try_into().unwrap();
            let offset = u32::from_le_bytes(offset_bytes);
            offset_vec.push(offset);
            if offset >= mkf_len {
                println!("last offset: {}, mkf len: {}", offset, mkf_len);
                break;
            }
            current_index += offset_len;
            if current_index as u32 >= mkf_len {
                break;
            }
        }

        let smkf_count = offset_vec.len() - 1;
        if smkf_count == 0 {
            panic!("failed to extract resource from {}", path);
        }

        let smkf_count = offset_vec.len() - 1;
        if smkf_count == 0 {
            panic!("failed to extract resource");
        }

        let mut smkf_list = Vec::new();
        for i in 0..smkf_count {
            let smkf_start = offset_vec[i] as usize;
            let smkf_end = offset_vec[i + 1] as usize;
            let smkf_content: Vec<u8> = mkf_content[smkf_start..smkf_end]
                .iter()
                .map(|x| *x)
                .collect();
            smkf_list.push(smkf_content);
        }
        println!("smkf len: {}", smkf_list.len());

        MKF {
            file_name: file_name.to_string(),
            smkf_list,
        }
    }
}
