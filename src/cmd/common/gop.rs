use super::file_reader;
use super::four_planes_reader::FourPlanes;
use super::pallete::Pallete;

pub fn extract(path: &String, prefix: &str, pallete: &Pallete, image_size: usize) {
    let buffer = file_reader::from(path);
    println!("image size: {}", image_size);
    let plane_width = image_size / 8;
    let plane_height = image_size;
    let plane_len = plane_width * plane_height;
    let block_len = plane_len * 4;
    let block_count = buffer.len() / block_len;
    println!(
        "buffer len: {}, block len: {}, block count: {}",
        buffer.len(),
        block_len,
        block_count
    );
    for i in 0..block_count {
        let buffer_start = i * block_len;
        let buffer_end = buffer_start + block_len;
        let four_planes = FourPlanes::new(
            plane_width as u16,
            plane_height as u16,
            format!("{}-{}", prefix, i + 1),
            &buffer[buffer_start..buffer_end],
        );
        four_planes.extract(pallete);
    }
}
