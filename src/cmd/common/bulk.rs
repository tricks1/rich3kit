use super::file_reader;
use super::four_planes_reader::FourPlanes;
use super::pallete::Pallete;
use std::convert::TryInto;

pub fn extract(path: &String, prefix: &str, bulk_filename: &str) {
    let pallete = Pallete::from(&format!("{}/16.PAT", path));
    let bulk_buffer = file_reader::from(&format!("{}/{}", path, bulk_filename));
    println!("bulk buffer length: {}", bulk_buffer.len());

    let width_bytes = &bulk_buffer[0..2];
    let plane_width = u16::from_le_bytes(width_bytes.try_into().unwrap());
    let height_bytes = &bulk_buffer[2..4];
    let plane_height = u16::from_le_bytes(height_bytes.try_into().unwrap());
    println!("plane width: {}, height: {}", plane_width, plane_height);

    let plane_len = (plane_width * plane_height) as usize;
    let block_len = 4 + plane_len * 4;
    let block_count = bulk_buffer.len() / block_len as usize;
    println!(
        "plane len: {}, block len: {}, block count: {}",
        plane_len, block_len, block_count
    );

    let image_width = plane_width as u32 * 8;
    let image_height = plane_height as u32;

    println!("image width: {}, height: {}", image_width, image_height);
    for i in 0..block_count {
        let plane_start = i * block_len as usize;
        let buffer_start = plane_start + 4;
        let buffer_end = buffer_start + block_len - 4;
        let four_planes = FourPlanes::new(
            plane_width,
            plane_height,
            format!("{}-{}", prefix, i + 1),
            &bulk_buffer[buffer_start..buffer_end],
        );
        four_planes.extract(&pallete);
    }
}
