use super::four_planes;
use super::four_planes::Buffer;
use super::mkf::MKF;
use super::pallete::Pallete;
use std::collections::HashMap;

pub const SCREEN_WIDTH: usize = 640;
pub const SCREEN_HEIGHT: usize = 400;
pub struct Screen {
    mkf: MKF,
    pallete: Pallete,
    method_map: MethodMap,
}

pub struct MethodMap {
    fn_map: HashMap<u8, fn(_: &mut Buffer, _: &Vec<u8>) -> bool>,
}

impl MethodMap {
    pub fn new() -> MethodMap {
        let mut map: HashMap<u8, fn(params: &mut Buffer, smkf_buffer: &Vec<u8>) -> bool> =
            HashMap::new();

        map.insert(0, |_, _| {
            // 解壓結束
            println!("extract complete");
            true
        });

        map.insert(1, |params, _| {
            // 輸出1 byte背景色
            params.add_bg_color_by_byte_count(1);
            params.increase_buffer_index_by(1);
            false
        });

        map.insert(2, |params, smkf| {
            // c = 讀取1 byte
            // 輸出c byte背景色
            let byte_count = params.get_current_u8(smkf, 1);
            params.add_bg_color_by_byte_count(byte_count as usize);
            params.increase_buffer_index_by(2);
            false
        });

        map.insert(3, |params, smkf| {
            // c = 讀取2 byte整數(低位元組在前)
            // 輸出c byte背景色
            let byte_count = params.get_current_u16(smkf, 1);
            params.add_bg_color_by_byte_count(byte_count as usize);
            params.increase_buffer_index_by(3);
            false
        });

        map.insert(4, |params, smkf| {
            // 讀取4 byte分別輸出至4 Plane(由低位元Plane開始)
            let start = params.get_current_buffer_index() + 1;
            let end = start + 4;
            params.copy_byte_to_each_plane(&smkf[start..end]);
            params.increase_buffer_index_by(5);
            false
        });

        map.insert(5, |params, smkf| {
            // c = 讀取1 byte
            // 分別讀取c byte輸出至4 Plane(由低位元Plane開始)
            let byte_count = params.get_current_u8(smkf, 1);
            let start = params.get_current_buffer_index() + 2;
            let end = start + byte_count as usize * 4;
            let buffer = &smkf[start..end];
            params.copy_byte_to_each_plane(buffer);
            params.increase_buffer_index_by(1 + 1 + buffer.len());
            false
        });
        map.insert(6, |params, smkf| {
            // c = 讀取2 byte整數(低位元組在前)
            // 分別讀取c byte輸出至4 Plane(由低位元Plane開始)
            let byte_count = params.get_current_u16(smkf, 1);
            let start = params.get_current_buffer_index() + 3;
            let end = start + byte_count as usize * 4;
            let buffer = &smkf[start..end];
            params.copy_byte_to_each_plane(buffer);
            params.increase_buffer_index_by(1 + 2 + buffer.len());
            false
        });
        map.insert(7, |params, smkf| {
            // b = 讀取1 byte
            // c = 讀取1 byte
            // 輸出8個顏色值b共c次
            let color_index = params.get_current_u8(smkf, 1);
            let byte_count = params.get_current_u8(smkf, 2);
            params.add_color_by_byte_count(color_index, byte_count as usize);
            params.increase_buffer_index_by(3);
            false
        });
        map.insert(8, |params, smkf| {
            // b = 讀取1 byte
            // c = 讀取2 byte整數(低位元組在前)
            // 輸出8個顏色值b共c次
            let color_index = params.get_current_u8(smkf, 1);
            let byte_count = params.get_current_u16(smkf, 2);
            params.add_color_by_byte_count(color_index, byte_count as usize);
            params.increase_buffer_index_by(4);
            false
        });
        map.insert(9, |params, smkf| {
            // d = 讀取1 byte
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料
            let offset = params.get_current_u8(smkf, 1);
            params.copy_self_by_offset(offset as usize, 1, 1);
            params.increase_buffer_index_by(2);
            false
        });
        map.insert(10, |params, smkf| {
            // d = 讀取1 byte
            // c = 讀取1 byte
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料c次
            let offset = params.get_current_u8(smkf, 1);
            let count = params.get_current_u8(smkf, 2);
            params.copy_self_by_offset(offset as usize, 1, count as usize);
            params.increase_buffer_index_by(3);
            false
        });
        map.insert(11, |params, smkf| {
            // d = 讀取1 byte
            // c = 讀取2 byte整數(低位元組在前)
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料c次
            let offset = params.get_current_u8(smkf, 1);
            let count = params.get_current_u16(smkf, 2);
            params.copy_self_by_offset(offset as usize, 1, count as usize);
            params.increase_buffer_index_by(4);
            false
        });
        map.insert(12, |params, smkf| {
            // d = 讀取2 byte整數(低位元組在前)
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料
            let offset = params.get_current_u16(smkf, 1);
            params.copy_self_by_offset(offset as usize, 1, 1);
            params.increase_buffer_index_by(3);
            false
        });
        map.insert(13, |params, smkf| {
            // d = 讀取2 byte整數(低位元組在前)
            // c = 讀取1 byte
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料c次
            let offset = params.get_current_u16(smkf, 1);
            let count = params.get_current_u8(smkf, 3);
            params.copy_self_by_offset(offset as usize, 1, count as usize);
            params.increase_buffer_index_by(4);
            false
        });
        map.insert(14, |params, smkf| {
            // d = 讀取2 byte整數(低位元組在前)
            // c = 讀取2 byte整數(低位元組在前)
            // 分別複製(4 Plane)在(目前位置-d)的1 byte資料c次
            let offset = params.get_current_u16(smkf, 1);
            let count = params.get_current_u16(smkf, 3);
            params.copy_self_by_offset(offset as usize, 1, count as usize);
            params.increase_buffer_index_by(5);
            false
        });
        map.insert(15, |params, smkf| {
            // d = 讀取1 byte整數
            // c = 讀取1 byte整數
            // 分別複製(4 Plane)在(目前位置-d)的c byte資料
            let offset = params.get_current_u8(smkf, 1);
            let byte_count = params.get_current_u8(smkf, 2);
            params.copy_self_by_offset(offset as usize, byte_count as usize, 1);
            params.increase_buffer_index_by(3);
            false
        });
        map.insert(16, |params, smkf| {
            // d = 讀取1 byte整數
            // c = 讀取2 byte整數(低位元組在前)
            // 分別複製(4 Plane)在(目前位置-d)的c byte資料
            let offset = params.get_current_u8(smkf, 1);
            let byte_count = params.get_current_u16(smkf, 2);
            params.copy_self_by_offset(offset as usize, byte_count as usize, 1);
            params.increase_buffer_index_by(4);
            false
        });
        map.insert(0x11, |params, smkf| {
            // d = 讀取2 byte整數(低位元組在前)
            // c = 讀取1 byte整數
            // 分別複製(4 Plane)在(目前位置-d)的c byte資料
            let offset = params.get_current_u16(smkf, 1);
            let byte_count = params.get_current_u8(smkf, 3);
            params.copy_self_by_offset(offset as usize, byte_count as usize, 1);
            params.increase_buffer_index_by(4);
            false
        });
        map.insert(0x12, |params, smkf| {
            // d = 讀取2 byte整數(低位元組在前)
            // c = 讀取2 byte整數(低位元組在前)
            // 分別複製(4 Plane)在(目前位置-d)的c byte資料
            let offset = params.get_current_u16(smkf, 1);
            let byte_count = params.get_current_u16(smkf, 3);
            params.copy_self_by_offset(offset as usize, byte_count as usize, 1);
            params.increase_buffer_index_by(5);
            false
        });
        map.insert(0x13, |params, _| {
            // 調整輸入緩衝區指標在32KB以內
            params.increase_buffer_index_by(1);
            false
        });
        MethodMap { fn_map: map }
    }
    pub fn read(&self, params: &mut Buffer, smkf_buffer: &Vec<u8>) {
        loop {
            let index = smkf_buffer[params.get_current_buffer_index()];
            let b = self.handle(&index, params, smkf_buffer);
            if b {
                break;
            }
        }
    }

    pub fn read_to_frames(
        &self,
        params: &mut Buffer,
        smkf_buffer: &Vec<u8>,
        pallete: &Pallete,
        name_prefix: &String,
    ) {
        let mut i = 0;
        loop {
            let index = smkf_buffer[params.get_current_buffer_index()];
            let b = self.handle(&index, params, smkf_buffer);
            let img = params.extract_image(pallete);
            img.save(format!("{}-{}.png", name_prefix, i)).unwrap();
            i += 1;
            if b {
                break;
            }
        }
    }

    fn handle(&self, index: &u8, params: &mut Buffer, smkf_buffer: &Vec<u8>) -> bool {
        self.fn_map.get(index).map_or_else(
            || {
                panic!("index: {} not set", index);
            },
            |f| f(params, smkf_buffer),
        )
    }
}

impl Screen {
    pub fn from(path: &String) -> Screen {
        let screen_path = format!("{}/SCREEN.MKF", path);
        let mkf = MKF::from(&screen_path);
        let pallete = Pallete::from(&format!("{}/16.PAT", path));
        let method_map = MethodMap::new();
        Screen {
            mkf,
            pallete,
            method_map,
        }
    }

    pub fn extract_to_frames(&self, index: usize) {
        self.mkf.get_smkf(index).map_or_else(
            || {
                panic!("screen index: {} not found", index);
            },
            |smkf_buffer| {
                let file_name = format!("screen-{}.png", index + 1);
                self.extract_smkf_frames(smkf_buffer, &self.pallete, file_name);
                println!("smkf size: {:#x}", smkf_buffer.len());
            },
        );
    }

    pub fn extract_all(&self) {
        for i in 0..self.mkf.get_smkf_len() {
            self.extract(i);
        }
    }

    fn extract(&self, index: usize) {
        self.mkf.get_smkf(index).map_or_else(
            || {
                panic!("screen index: {} not found", index);
            },
            |smkf_buffer| {
                let file_name = format!("screen-{}.png", index + 1);
                self.extract_smkf(smkf_buffer, &self.pallete, file_name);
                println!("smkf size: {:#x}", smkf_buffer.len());
            },
        );
    }

    fn extract_smkf(&self, smkf_buffer: &Vec<u8>, pallete: &Pallete, file_name: String) {
        let mut params_1 = Buffer::new(SCREEN_WIDTH, SCREEN_HEIGHT);
        self.method_map.read(&mut params_1, smkf_buffer);
        let mut params_2 = Buffer::new(SCREEN_WIDTH, SCREEN_HEIGHT);
        params_2.set_bg_color_index(5);
        self.method_map.read(&mut params_2, smkf_buffer);

        let image_1 = params_1.extract_image(pallete);
        let image_2 = params_2.extract_image(pallete);
        four_planes::extract_from_2_image_buffers(&image_1, &image_2, file_name);
    }

    fn extract_smkf_frames(&self, smkf_buffer: &Vec<u8>, pallete: &Pallete, file_name: String) {
        let mut params_1 = Buffer::new(SCREEN_WIDTH, SCREEN_HEIGHT);
        self.method_map
            .read_to_frames(&mut params_1, smkf_buffer, pallete, &file_name);
    }
}
