use super::four_planes::Buffer;
use std::collections::HashMap;
use std::convert::TryInto;

struct Params {
    back: bool,
    return_index: usize,
    buffer: Buffer,
}

impl Params {
    pub fn new(image_width: usize, image_height: usize) -> Params {
        Params {
            back: false,
            return_index: 0,
            buffer: Buffer::new(image_width, image_height),
        }
    }
}
pub struct SpriteReader {
    method_map: HashMap<u8, fn(_: &mut Params, _: &Vec<u8>) -> bool>,
}

impl SpriteReader {
    pub fn get_four_planes_buffer(&self, content: &Vec<u8>, bg_color_index: u8) -> Buffer {
        let image_width = u16::from_le_bytes(content[0..2].try_into().unwrap()) as usize * 8;
        let image_height = u16::from_le_bytes(content[2..4].try_into().unwrap()) as usize;
        let mut params = Params::new(image_width, image_height);
        let mut count_map = HashMap::new();
        params.buffer.set_current_buffer_index(4);
        params.buffer.set_bg_color_index(bg_color_index);
        params.buffer.fill_bg_color_and_reset();
        loop {
            let id = params.buffer.get_current_u8(content, 0);
            let counter = count_map.entry(id).or_insert(0);
            *counter += 1;
            /*
            println!(
                "id: {}, index: {}",
                id,
                params.buffer.get_current_buffer_index()
            );
            */
            let need_break = self.method_map.get(&id).map_or_else(
                || {
                    panic!("id {} is not implemented", id);
                },
                |x| x(&mut params, content),
            );
            params.buffer.save_frame();
            if need_break {
                break;
            }
        }
        //println!("count map: {:?}", count_map);
        params.buffer
    }
    pub fn new() -> SpriteReader {
        let mut method_map: HashMap<u8, fn(_: &mut Params, _: &Vec<u8>) -> bool> = HashMap::new();

        method_map.insert(0x0, |_, _| {
            // 00: 解壓結束
            println!("extract complete");
            true
        });

        method_map.insert(0x1, |params, _| {
            // 01: if back then
            // back = false
            // 檔案移到ret_p的位置
            // end if
            // 換下一水平線 (y+=1, x-=xsize)
            let buffer = &mut params.buffer;
            if params.back {
                params.back = false;
                buffer.set_current_buffer_index(params.return_index - 1);
                println!("back to {}", params.return_index);
            }

            buffer.next_line();

            buffer.increase_buffer_index_by(1);
            false
        });

        method_map.insert(0x2, |params, _| {
            // 02: 輸出1 byte背景色
            params.buffer.add_bg_color_by_byte_sprite(1);
            params.buffer.increase_buffer_index_by(1);
            false
        });

        method_map.insert(0x3, |params, file| {
            // 03: c = 讀取1 byte
            // 輸出c byte背景色
            let buffer = &mut params.buffer;
            let byte_count = buffer.get_current_u8(file, 1);
            buffer.add_bg_color_by_byte_sprite(byte_count as usize);
            buffer.increase_buffer_index_by(2);
            false
        });

        method_map.insert(0x4, |params, file| {
            // 04: mask = 讀取1 byte
            // 分別讀取1 byte按mask值(位元1才輸出)輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let mask = buffer.get_current_u8(file, 1);
            let start = buffer.get_current_buffer_index() + 2;
            let end = start + 4;
            buffer.copy_byte_to_each_plane_with_mask(&file[start..end], mask);
            buffer.increase_buffer_index_by(6);
            false
        });

        method_map.insert(0x5, |params, file| {
            // 05: c = 讀取1 byte
            // 執行下列c次:
            // mask = 讀取1 byte
            // 分別讀取1 byte按mask值(位元1才輸出)輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let count = buffer.get_current_u8(file, 1);
            buffer.increase_buffer_index_by(2);
            for _ in 0..count {
                let mask = buffer.get_current_u8(file, 0);
                let start = buffer.get_current_buffer_index() + 1;
                let end = start + 4;
                buffer.copy_byte_to_each_plane_with_mask(&file[start..end], mask);
                buffer.increase_buffer_index_by(5);
            }
            false
        });

        method_map.insert(0x6, |params, file| {
            // 06: 分別讀取1 byte輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let start = buffer.get_current_buffer_index() + 1;
            let end = start + 4;
            buffer.copy_byte_to_each_plane(&file[start..end]);
            buffer.increase_buffer_index_by(5);
            false
        });

        method_map.insert(0x7, |params, file| {
            // 07: c = 讀取1 byte
            // 分別讀取c byte輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let count = buffer.get_current_u8(file, 1) as usize;
            let start = buffer.get_current_buffer_index() + 2;
            let end = start + count * 4;
            buffer.copy_byte_to_each_plane(&file[start..end]);
            buffer.increase_buffer_index_by(2 + count * 4);
            false
        });

        method_map.insert(0x8, |params, file| {
            // 08: b = 讀取1 byte
            // c = b高4位元+1
            // b = b低4位元
            // 輸出8個顏色值b共c次
            let buffer = &mut params.buffer;
            let val = buffer.get_current_u8(file, 1);
            let color_index = val & 0xf;
            let byte_count = (val >> 4) + 1;
            buffer.add_color_by_byte_count(color_index, byte_count as usize);
            buffer.increase_buffer_index_by(2);
            false
        });

        method_map.insert(0x9, |params, file| {
            // 09: c = 讀取1 byte+1
            // b = 讀取1 byte
            // 輸出8個顏色值b共c次
            let buffer = &mut params.buffer;
            let byte_count = buffer.get_current_u8(file, 1) + 1;
            let color_index = buffer.get_current_u8(file, 2);
            buffer.add_color_by_byte_count(color_index, byte_count as usize);
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0xa, |params, file| {
            // 0A: 分別讀取1 byte輸出至4 Plane兩次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let start = buffer.get_current_buffer_index() + 1;
            let end = start + 4;
            for _ in 0..2 {
                buffer.copy_byte_to_each_plane(&file[start..end])
            }
            buffer.increase_buffer_index_by(5);
            false
        });

        method_map.insert(0xb, |params, file| {
            // 0B: 分別讀取1 byte輸出至4 Plane三次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let start = buffer.get_current_buffer_index() + 1;
            let end = start + 4;
            for _ in 0..3 {
                buffer.copy_byte_to_each_plane(&file[start..end])
            }
            buffer.increase_buffer_index_by(5);
            false
        });

        method_map.insert(0xc, |params, file| {
            // 0C: 分別讀取1 byte輸出至4 Plane四次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let start = buffer.get_current_buffer_index() + 1;
            let end = start + 4;
            for _ in 0..4 {
                buffer.copy_byte_to_each_plane(&file[start..end])
            }
            buffer.increase_buffer_index_by(5);
            false
        });

        method_map.insert(0xd, |params, file| {
            // 0D: 讀取4 byte
            // c = 讀取1 byte+1
            // 將前面讀取的4 byte分別輸出至4 Plane c次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let start = buffer.get_current_buffer_index() + 1;
            let end = start + 4;
            let count = buffer.get_current_u8(file, 5) + 1;
            for _ in 0..count {
                buffer.copy_byte_to_each_plane(&file[start..end])
            }
            buffer.increase_buffer_index_by(6);
            false
        });

        method_map.insert(0xe, |params, file| {
            // 0E: p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // ret_p = 檔案目前位置
            // back = true
            // 檔案移到p-d的位置
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1);
            let current_index = buffer.get_current_buffer_index() + 1;
            params.return_index = buffer.get_current_buffer_index() + 3;
            params.back = true;
            buffer.set_current_buffer_index(current_index - offset as usize);
            println!("move to {}", current_index - offset as usize);
            false
        });

        method_map.insert(0xf, |params, file| {
            // 0F: mask = 讀取1 byte
            // b = 讀取1 byte
            // c = b高4位元
            // b = b低4位元
            // 按mask值(位元1才輸出)輸出8個顏色值b共c次
            let buffer = &mut params.buffer;
            let mask = buffer.get_current_u8(file, 1);
            let val = buffer.get_current_u8(file, 2);
            let color_count = val >> 4;
            let color_index = val & 0xf;
            buffer.add_color_by_byte_count_with_mask(color_index, mask, color_count as usize);
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x10, |params, file| {
            // 10: c = 讀取1 byte
            // mask = 讀取1 byte
            // b = 讀取1 byte
            // 按mask值(位元1才輸出)輸出8個顏色值b共c次
            let buffer = &mut params.buffer;
            let byte_count = buffer.get_current_u8(file, 1);
            let mask = buffer.get_current_u8(file, 2);
            let color = buffer.get_current_u8(file, 3);
            buffer.add_color_by_byte_count_with_mask(color, mask, byte_count as usize);
            buffer.increase_buffer_index_by(4);
            false
        });

        method_map.insert(0x11, |params, file| {
            // 11: p = 目前檔案位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)分別讀取1 byte輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let start = buffer.get_current_buffer_index() + 1 - offset;
            let end = start + 4;
            buffer.copy_byte_to_each_plane(&file[start..end]);
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x12, |params, file| {
            // 12: p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)分別讀取1 byte輸出至4 Plane兩次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let start = buffer.get_current_buffer_index() + 1 - offset;
            let end = start + 4;
            for _ in 0..2 {
                buffer.copy_byte_to_each_plane(&file[start..end]);
            }
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x13, |params, file| {
            // 13: p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)分別讀取1 byte輸出至4 Plane三次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let start = buffer.get_current_buffer_index() + 1 - offset;
            let end = start + 4;
            for _ in 0..3 {
                buffer.copy_byte_to_each_plane(&file[start..end]);
            }
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x14, |params, file| {
            // 14: p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)分別讀取1 byte輸出至4 Plane四次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let start = buffer.get_current_buffer_index() + 1 - offset;
            let end = start + 4;
            for _ in 0..4 {
                buffer.copy_byte_to_each_plane(&file[start..end]);
            }
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x15, |params, file| {
            //  p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // c = 讀取1 byte+1
            // 到(p-d)分別讀取1 byte輸出至4 Plane c次(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let count = buffer.get_current_u8(file, 3) as usize + 1;
            let start = buffer.get_current_buffer_index() + 1 - offset;
            let end = start + 4;
            for _ in 0..count {
                buffer.copy_byte_to_each_plane(&file[start..end]);
            }
            buffer.increase_buffer_index_by(4);
            false
        });

        method_map.insert(0x16, |params, file| {
            // 16: p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d):
            // mask = 讀取1 byte
            // 分別讀取1 byte按mask值(位元1才輸出)輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let offset = buffer.get_current_u16(file, 1) as usize;
            let mask_index = buffer.get_current_buffer_index() + 1 - offset;
            let mask = buffer.get_current_u8_at(file, mask_index);
            let start = mask_index + 1;
            let end = start + 4;
            buffer.copy_byte_to_each_plane_with_mask(&file[start..end], mask);
            buffer.increase_buffer_index_by(3);
            false
        });

        method_map.insert(0x17, |params, file| {
            // 17: c = 讀取1 byte
            // p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)並執行下列c次:
            // mask = 讀取1 byte
            // 分別讀取1 byte按mask值(位元1才輸出)輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let count = buffer.get_current_u8(file, 1) as usize;
            let offset = buffer.get_current_u16(file, 2) as usize;

            for i in 0..count {
                let mask_index = buffer.get_current_buffer_index() + 2 - offset + i * 5;
                let mask = buffer.get_current_u8_at(file, mask_index);
                let start = mask_index + 1;
                let end = start + 4;
                buffer.copy_byte_to_each_plane_with_mask(&file[start..end], mask);
            }
            buffer.increase_buffer_index_by(4);
            false
        });

        method_map.insert(0x18, |params, file| {
            // 18: c = 讀取1 byte
            // p = 檔案目前位置
            // d = 讀取2 byte整數(低位元組在前)
            // 到(p-d)分別讀取c byte輸出至4 Plane(由低位元Plane開始)
            let buffer = &mut params.buffer;
            let count = buffer.get_current_u8(file, 1) as usize;
            let offset = buffer.get_current_u16(file, 2) as usize;

            let start = buffer.get_current_buffer_index() + 2 - offset;
            let end = start + 4 * count;
            buffer.copy_byte_to_each_plane(&file[start..end]);
            buffer.increase_buffer_index_by(4);
            false
        });
        SpriteReader { method_map }
    }
}
