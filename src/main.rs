use std::env;

pub mod cmd;
use cmd::CmdMap;
fn main() {
    let args = env::args().collect();
    let cmd_map = CmdMap::new();
    cmd_map.run(&args);
}
