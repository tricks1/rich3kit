use rich3kit::cmd::common::four_planes::Buffer;
use rich3kit::cmd::common::pallete::Pallete;
#[test]
fn it_works() {
    assert_eq!(1, 1);
}

#[test]
fn add_mask_color() {
    let mut buffer = Buffer::new(8, 1);
    buffer.set_bg_color_index(3);
    buffer.fill_bg_color_and_reset();
    buffer.add_color_by_byte_count_with_mask(2, !0x1f, 1);
    const PALLETE_PATH: &str = "../../OneDrive/dosbox/rich3";
    let pallete = Pallete::from(&format!("{}/16.PAT", PALLETE_PATH));
    buffer.extract_and_save(&pallete, String::from("test"));
}
